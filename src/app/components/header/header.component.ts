import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() onClickedButton= new EventEmitter();
  @Output() showPanel= new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  showHidPageMain(){
  this.onClickedButton.emit();
  }
  showConverPanel(){
    this.showPanel.emit();
  }
}
