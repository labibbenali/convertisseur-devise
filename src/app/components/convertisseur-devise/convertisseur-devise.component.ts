import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-convertisseur-devise',
  templateUrl: './convertisseur-devise.component.html',
  styleUrls: ['./convertisseur-devise.component.scss']
})
export class ConvertisseurDeviseComponent implements OnInit {

  APIkey:string="b5ff7110-838d-11ec-b72b-7b6052180b2d";
  from:string="EUR";
  optionFromCurrency!:string;
  optionTocurrency!:string;
  typedAmount!:number;
  result!:string;
  post!:any;
  convert!:any;

  constructor(public http:HttpClient ) {}

  ngOnInit(): void {
    this.getData(this.APIkey,this.from);
  }
//Http client to get data
  getData(apiKey:string, from:string){
    this.http.get(this.getUrl(apiKey,from))
      .subscribe(response => {
        this.post=response;
        this.convert=this.post;
        this.post=Object.keys(this.post.data);
      })
  }
  //set up the url of the api apikey is the key of user and from is the currency that
  // you want to convert
  getUrl(APIkey:string,from:string){
    return `https://freecurrencyapi.net/api/v2/latest?apikey=
    ${APIkey}&base_currency=${from}`
  }
  getCurrencyFrom(event:any){
    this.optionFromCurrency=event.target.value;
    if(this.optionFromCurrency !="EUR"){
      this.getData(this.APIkey,this.optionFromCurrency);
    }
    console.log(this.optionFromCurrency);
  }
  getCurrencyTo(event:any){
   this.optionTocurrency=event.target.value;
   console.log(this.optionTocurrency);
  }
// it must the two currency be chosen and the amount to enable the button
  statusButton(){
    return !(this.optionFromCurrency && this.typedAmount && this.optionTocurrency)
  }
  //action of button get and calculate the value
  showText() {
    if(this.optionFromCurrency==this.optionTocurrency){
      this.result=(this.typedAmount).toString();
    } else{
      this.result=(this.convert.data[this.optionTocurrency]*
        this.typedAmount).toFixed(3);
    }
  }
}
